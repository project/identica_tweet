<?php

/**
 * @file
 *   Admin settings for identica_tweet module.
 */

function identica_tweet_admin() {
  $image_location = drupal_get_path('module', 'identica_tweet') .'/identica.png';
  $form['identica_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image'),
    '#description' => t('The location of the icon to use with the "Post to Identi.ca" link, relative to your Drupal installation. Ex.: @location', array('@location' => $image_location)),
    '#default_value' => variable_get('identica_image', $image_location),
  );
  return system_settings_form($form);
}
